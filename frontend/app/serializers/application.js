import DS from 'ember-data';
// import { get } from '@ember/object';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    return this._super(store, primaryModelClass, payload.data, id, requestType);
  }
  // Serialize only new records or dirty attributes by default
  // serializeAttribute(snapshot, json, key) {
  //   if (get(snapshot.record, 'isNew') || snapshot.changedAttributes()[key]) {
  //     this._super(...arguments);
  //   }
  // }
});
