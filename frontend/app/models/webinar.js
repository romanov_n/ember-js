import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  title: DS.attr('string'),
  description: DS.attr('string'),
  url: DS.attr('string'),
  state: DS.attr('number'),
  user: DS.attr(),
  imageStyle: computed(function() {
    return "background-image:url('" + `https://img.youtube.com/vi/` + this.get('url') + `/hqdefault.jpg` + "')"
  })
});
