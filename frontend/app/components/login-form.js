import Component from '@ember/component';
import Ember from 'ember';
import { buildValidations, validator } from 'ember-cp-validations';
import { task } from 'ember-concurrency';

const {
  inject: { service },
  get,
  getProperties,
  setProperties
} = Ember;

const Validations = buildValidations({
  identification: {
    description: 'Email',
    validators: [
      validator('presence', true),
      validator('format', {
        allowBlank: true,
        type: 'email'
      })
    ]
  },
  password: {
    description: 'Password',
    validators: [
      validator('presence', true)
    ]
  }
});

export default Component.extend(Validations, {
  session: service('session'),
  authError: null,

  emailAuthentication: task(function* () {
    this.set('authError', null);
    let credentials = getProperties(this, 'identification', 'password');

    if (get(this, 'validations.isValid')) {
      try {
        yield get(this, 'session').authenticate('authenticator:token', credentials);
      } catch(e) {
        this.set('authError', 'User does not exist');
      }
    } else {
      this.set('authError', 'Incorrect input data');
    }
  }).drop()
});
