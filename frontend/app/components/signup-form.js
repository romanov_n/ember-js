import Component from '@ember/component';
import Ember from 'ember';
import {buildValidations, validator} from 'ember-cp-validations';
import {task} from 'ember-concurrency';

const {inject: {service}} = Ember;

const Validations = buildValidations({
  email: {
    description: 'Email',
    validators: [
      validator('presence', true),
      validator('format', {
        allowBlank: true,
        type: 'email'
      })
    ]
  },
  password: {
    description: 'Password',
    validators: [
      validator('presence', true)
    ]
  },
  confirmPassword: {
    description: 'Password',
    validators: [
      validator('presence', true)
    ]
  },
  name: {
    description: 'Name',
    validators: [
      validator('presence', true)
    ]
  }
});

export default Component.extend(Validations, {
  session: service('session'),
  store: service(),
  inputErrors: null,

  userCreating: task(function* () {
    let properties = this.getProperties('email', 'name', 'password', 'confirmPassword');

    let errors = [];
    let email = properties.email;
    let name = properties.name;
    let password = properties.password;
    let confirmPassword = properties.confirmPassword;

    if (this.get('validations.isValid')) {
      if (password !== confirmPassword) {
        errors.push('Passwords do not match')
      } else {
        try {
          let new_user = this.get('store').createRecord('user', {email: email, name: name, password: password});
          new_user.save()
        } catch (e) {
          errors.push('Error while creating user: ' + e);
        }
      }
    } else {
      errors.push('Please enter all fields');
    }
    this.set('inputErrors', errors);
  }).drop()
});
