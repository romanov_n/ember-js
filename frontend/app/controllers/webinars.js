import Controller from '@ember/controller';
import { inject } from '@ember/service';

const {computed} = Ember;

export default Controller.extend({
  session: inject('session'),
  currentUser: inject('current-user'),

  my_webinars: computed(function() {
      let model = this.get('model');
      let current = this.get('currentUser');
      return model.filter(function(item) {
          let user_data = item.get('user');
          return user_data.id == current.user.id;
      });
  })
});
