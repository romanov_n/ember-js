class UsersController < ApplicationController
  skip_before_action :authenticate, only: [:create]

  def index
    query = "{
      user(email: \"#{param[:filter][:email]}\") {
        id
        email
        name
      }
    }"
    render json: BackendSchema.execute(query)
  end

  def show
    query = "{
      user(id: #{param[:id]}) {
        id
        email
        name
      }
    }"
    render json: BackendSchema.execute(query)
  end

  def create
    user_params = params[:user]
    query = "{
      create_user(email: \"#{user_params[:email]}\",
                  name: \"#{user_params[:name]}\",
                  password: \"#{user_params[:password]}\") {
        id
        email
        name
      }
    }"
    render json: BackendSchema.execute(query)
  end

  def current
    query = "{
      user(id: #{current_user.id}) {
        id
        email
        name
      }
    }"
    render json: BackendSchema.execute(query)
  end
end
