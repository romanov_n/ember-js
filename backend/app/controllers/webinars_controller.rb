class WebinarsController < ApplicationController
  def index
    query = "{
      webinars {
        id
        description
        url
        state
        user {
          id
          email
          name
          type
        }
      }
    }"
    render json: BackendSchema.execute(query)
  end

  def show
    query = "{
      webinar(id: #{params[:id]}) {
        id
        description
        url
        state
        user {
          id
          email
          name
          type
        }
      }
    }"
    render json: BackendSchema.execute(query)
  end
end
