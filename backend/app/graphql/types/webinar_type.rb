module Types
  class WebinarType < Types::BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :description, String, null: false
    field :url, String, null: false
    field :state, Integer, null: false
    field :user, Types::UserType, null: false

    def type
      'webinar'
    end
  end
end
