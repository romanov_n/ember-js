module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :email, String, null: false
    field :type, String, null: false

    def type
      'user'
    end
  end
end
