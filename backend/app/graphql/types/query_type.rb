module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # /webinars
    field :webinars, [Types::WebinarType], null: true, description: "Returns all webinars"
    def webinars
      Webinar.all
    end

    # /webinars/:id
    field :webinar, Types::WebinarType, null: true, description: "Return webinar" do
      argument :id, ID, required: true
    end
    def webinar(id:)
      Webinar.find(id)
    end

    # /users
    field :users, [Types::UserType], null: true, description: "Return all users"
    def users
      User.all
    end

    # /user/:id
    field :user, Types::UserType, null: true, description: "Return user" do
      argument :id, ID, required: false
      argument :email, String, required: false
    end
    def user(**attributes)
      User.find_by(**attributes)
    end
  end
end
