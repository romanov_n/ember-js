module Mutations
  class CreateUser < ::Mutations::BaseMutation
    argument :name, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true

    type Types::UserType

    def resolve(**attributes)
      Users.create!(attributes)
    end
  end
end