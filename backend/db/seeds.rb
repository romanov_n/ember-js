# FactoryGirl.create(:user_with_boards,
#   email: 'admin@example.com',
#   password: 'password',
#   boards_count: 3,
#   columns_count: 3,
#   tasks_count: 7
# )

# Create Users
User.create(name: 'User 1', email:'ex1@mail.ru', password: '1111')
User.create(name: 'User 2', email:'ex2@mail.ru', password: '1111')
User.create(name: 'User 3', email:'ex3@mail.ru', password: '1111')

# Create Webinars
url = 'Mmp9YmqkjYI'
Webinar.create(title: 'Webinar 1', description: 'Description 1', url: url, state: 0, user_id: 1)
Webinar.create(title: 'Webinar 2', description: 'Description 2', url: url, state: 0, user_id: 1)
Webinar.create(title: 'Webinar 3', description: 'Description 3', url: url, state: 0, user_id: 2)
Webinar.create(title: 'Webinar 4', description: 'Description 4', url: url, state: 0, user_id: 2)
Webinar.create(title: 'Webinar 5', description: 'Description 5', url: url, state: 0, user_id: 3)
