class CreateWebinars < ActiveRecord::Migration[6.0]
  def change
    create_table :webinars do |t|
      t.string :title, null: false
      t.string :description, default: '', null: false
      t.string :url, null: false
      t.integer :state, default: 0, null: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
