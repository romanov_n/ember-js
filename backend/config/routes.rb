Rails.application.routes.draw do
  post "/graphql", to: "graphql#execute"
  mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"

  resources :webinars

  resources :users, only: [:show, :index, :create] do
    get :current, on: :collection
  end

  resources :sessions, only: [:create] do
    delete  :destroy, on: :collection, as: :logout
  end
end
